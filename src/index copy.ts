/* eslint-disable */
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as twilio from "twilio";
// import fetch = require('node-fetch');
const fetch = require('node-fetch');
// const curl = require('curl');
var myRequest = require('request');
const requestPromise = require('request-promise');
// const request = require('postman-request');

// let ParseHub = require('parsehubv2');
// let phub = new ParseHub('Ytt0O7QxTTa7H');
// const postmanRequest = require('postman-request');
// const connectionManager = require('nm-vpn');

// Both versions
const BitlyClient = require('bitly').BitlyClient;
const bitly = new BitlyClient('fd56475c3e14efbf0803ff77dcdd99f24ec92956');

const cors = require('cors')({origin: true});

const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyB32PqWU-eCNcqFuUuXFI5XKmFd0E7ntpg",
    authDomain: "direct-from-dubai.firebaseapp.com",
    databaseURL: "https://direct-from-dubai.firebaseio.com",
    projectId: "direct-from-dubai",
    storageBucket: "direct-from-dubai.appspot.com",
    messagingSenderId: "191492841609",
    appId: "1:191492841609:web:0c52e7943fc4b2ac",
  },
  stripeApiKey: "pk_test_2A9qIsc24z965nEycOKO3HVe",
  twilio: {
    accountSid: "AC624cc591d8bb747188f60870386cf434",
    authToken: "679a7409fb7c160bea547e3d131f06bf",
  },
  parsehub: {
    PARSEHUB_API_KEY: "tt0O7QxTTa7H",
  },
}

admin.initializeApp(environment.firebase);

const accountSid = functions.config().twilio.sid;
const dfdapprovednumber = functions.config().twilio.dfdapprovednumber;
const token = functions.config().twilio.token;
const client = twilio(accountSid, token, {lazyLoading: true});

exports.sendCarToCust = functions.firestore.document("carsSent/{carSentId}")
    .onCreate((snap, context) => {
      const sendToMany:[] = snap.get("sendToMany");
      if (sendToMany) {
        sendToMany.forEach( (eachUser) => {
          const userMan: any = eachUser;
          const offerCarMessagae = {
            body: `Dear ${snap.get("buyerName")}, you have a new message: Here is a ${snap.get("modelYear")} ${snap.get("make")} ${snap.get("model")} in ${snap.get("bodyColor")} with ${snap.get("mileageMiles")} Miles at ${parseInt(snap.get("basePrice"), 10)} ${snap.get("currency")} ${snap.get("description") || ""}.  ~ ${snap.get("link")}`,
            to: `whatsapp:${userMan.buyerPhone}`,
            from: `whatsapp:${dfdapprovednumber}`,
          };
          client.messages.create(offerCarMessagae)
              .then( (messagae: any) => console.log(messagae))
              .catch( (errorae : any) => console.log(errorae));
        });
      } else {
        const offerCarMessage = {
          body: `Dear ${snap.get("buyerName")}, you have a new message: Here is a ${snap.get("modelYear")} ${snap.get("make")} ${snap.get("model")} in ${snap.get("bodyColor")} with ${snap.get("mileageMiles")} Miles at ${parseInt(snap.get("basePrice"), 10)} ${snap.get("currency")} ${snap.get("description") || ""}.  ~ ${snap.get("link")}`,
          to: `whatsapp:${snap.get("buyerPhone")}`,
          from: `whatsapp:${dfdapprovednumber}`,
        };
        client.messages.create(offerCarMessage)
            .then( (message: any) => console.log(message))
            .catch( (error: any) => console.log(error));
      }
    });

exports.whatsappNewSignUp = functions.firestore.document(`users/{userId}`)
    .onCreate( (snap, context) => {

      if(snap.get('funnel')==='lead'){

      console.log(`${snap.get('displayName')} should not receive message yet`)
      ///// UPDATE NOTES ////////

      let myNote = {
        note: snap.get('lastMessage'),
        link: snap.get('linkIncluded') || "",
        actions: ['Received Enquiry'],
        admin:  'Gerald Makokha',
        customerId: snap.id,
        createdDate: snap.get('createdDate'),
        isInbound: snap.get('isInbound'),
        channels: ['ebay','Email'],
        includeLink: snap.get('linkIncluded') !='' ? true : false,
        other: ''
      }

      updateNotesForNewLeads(myNote)
      .then( resp => {

        console.log("NOTESSSSSSSSS")

      }).catch( err => {

        console.log("NOTESSSSSSSSS ERROR: ", err)

      })



      }else {

        const userWhatsappMessage = {
          body:`Congratulations ${snap.data().displayName}, your account with DirectFromDubai has been created successfully. We will get back to you soon reference ${snap.get('make')} ${snap.get('model')} ${snap.get('variant') !=='' ? snap.get('variant'):''} Sign in to your account using the following link: ${snap.get('signInLink') ? snap.get('signInLink').substring(8) : 'directfromdubai.com'} Username: ${snap.get('email')}, Password: ${snap.get('password')}` ,
          to:`whatsapp:${snap.data().phone}`,
          from:`whatsapp:${dfdapprovednumber}`,
        }
        
        const newSignupMessageAdmin = {
            body:`Hello mate, ${snap.get('displayName')} has just signed up with Direct From Dubai & is looking for ${snap.get('make')} ${snap.get('model')}. Kindly get in touch using: Email ${snap.get('email')} and Phone: ${snap.get('phone')}`,
            to: `whatsapp:+447306195775`,
            from: `whatsapp:${dfdapprovednumber}`,
        }
        
  
        client.messages.create(userWhatsappMessage).then( (message: any) => console.log(message)).catch( (error: any) => console.log(error))
        client.messages.create(newSignupMessageAdmin).then( (message: any) => console.log(message)).catch( (error: any) => console.log(error))

      }
  
    });

exports.incomingMessageFromTwilio = functions.https.onRequest((request, response) => {
  cors( request, response, () => {
    
        const content = request.body
        const phone = content.From.substring(9,22)
    
        admin.firestore().collection('users').get()
        .then( res => {
    
          /////////////////// START //////////////////
          const customerWithPhoneNumber = res.docs.find( myDoc => myDoc.get('phone') === phone)
    
          const myTimeNow  = Date.parse(new Date().toUTCString())
          
    
          const customerChat =  {
    
            status: content.SmsStatus,
            subject:'enquiry',
            carId: 'noCarIdYet',
            pictures: ['https://www.directfromdubai.com/assets/img/no-user.png'],
            phone: phone,
            name: customerWithPhoneNumber?.get('displayName'),
            message: content.Body,
            isRead: false,
            isAdmin: false,
            email: customerWithPhoneNumber?.get('email'),
            customerId: customerWithPhoneNumber?.id,
            createdAt: myTimeNow,
            mediaUrl: content.MediaUrl0 || '',
            mediaType: content.MediaContentType0 || 'text',
      
          }
    
          const myLastMessage = {
    
            createdAt: myTimeNow,
            email: customerWithPhoneNumber?.get('email'),
            isRead: false,
            name: customerWithPhoneNumber?.get('displayName'),
            isActive: false,
            lastMessage: content.Body,
            unreadCounter: 1,
            mediaUrl: content.MediaUrl0 || '',
            mediaType: content.MediaContentType0 || 'text',
            // unreadCounter: unreadMessagesCount(customerWithPhoneNumber?.get('email')),
            userId: customerWithPhoneNumber?.id,
          }
    
          saveIncommingMessage(myLastMessage)
          updateMyChat(customerChat)
    
          /////////////////// STOP ///////////////////
          
          
        })
        .catch(error =>  response.send(error))
    
      })
    });

function updateMyChat(message: any) {
  message.token = 'fromPhone'

  admin.firestore().collection('customerEngagements').doc(message.email).collection('chats').add(message)
  .then(doc => {
        console.log("Updated Messages")
      })
  .catch( error => {
        console.log("Error Updating  Messages", error)
      })
  }

function saveIncommingMessage(message: any) {

  admin.firestore().collection('customerEngagements').doc(message.email).set(message)
  .then(doc => console.log("Message Saved"))
  .catch( error =>  console.log("Error adding Eng ", error))

  }


 /// DELETING CUSTOMER ///
 exports.deleteCustomer = functions.firestore.document(`users/{userId}`)
 .onDelete(snap => {
   
  return admin.auth().deleteUser(snap.id)
   .then(() => {
     console.log('Successfully deleted ', snap.get('displayName'));
   })
   .catch((error) => {
     console.log('Error deleting user:', error);
   });
 
 })


 exports.whatsappEuropeCustomerOnInquiry = functions.firestore.document(`customerEngagements/{messageId}/chats/{chatId}`)
        .onCreate((snap, context) => {

          admin.firestore().collection('users').doc(snap.get('customerId')).get()
            .then( res =>{

              const iamAdmin: boolean = snap.get('isAdmin')


              if(iamAdmin){

                if(snap.get('file')){

                  const newMessage = {
                    body:`${snap.get('message')}`,
                    mediaUrl:`${snap.get('file')}`,
                    to: `whatsapp:${res.get('phone')}`,
                    statusCallback: `https://us-central1-direct-from-dubai.cloudfunctions.net/updateMessageDeliveryStatus?identity=${snap.id}&email=${snap.get('email')}`,
                    from: `whatsapp:${dfdapprovednumber}`,
                  }
        
                 client.messages.create(newMessage)
                    .then( (message: any) => console.log(message))
                    .catch( (error: any) => console.log(error))

                }else {
                  const newMessage = {
                    body:`${snap.get('message')}`,
                    to: `whatsapp:${res.get('phone')}`,
                    statusCallback: `https://us-central1-direct-from-dubai.cloudfunctions.net/updateMessageDeliveryStatus?identity=${snap.id}&email=${snap.get('email')}`,
                    from: `whatsapp:${dfdapprovednumber}`,
                  }
        
                 client.messages.create(newMessage)
                    .then( (message: any) => console.log(message))
                    .catch( (error: any) => console.log(error))
                }
    
              }else if(!iamAdmin){
                
                if(snap.get('subject') === 'order'){
    
                  const orderMessage = {
                    body:`Thank you ${snap.get('name')} for placing an order on ${snap.get('carTitle')}. One of our sales team will get back to you soon.`,
                    to: `whatsapp:${res.get('phone')}`,
                    // statusCallback: `https://us-central1-direct-from-dubai.cloudfunctions.net/updateMessageDeliveryStatus?identity=${snap.id}&email=${snap.get('email')}`,
                    from: `whatsapp:${dfdapprovednumber}`,
                  }

                  const orderMessageAdmin = {
                    // Hello mate, {{1}} ordered the {{2}} at  {{3}}. Kindly get in touch on phone: {{4}} or email: {{5}}
                    body:`Hello mate, ${snap.get('name')} ordered the ${snap.get('carTitle')} at  ${snap.get('gbp')}GBP. Kindly get in touch on phone: ${snap.get('phone')} or email: ${snap.get('email')}`,
                    to: `whatsapp:+971526186549`,
                    // statusCallback: `https://us-central1-direct-from-dubai.cloudfunctions.net/updateMessageDeliveryStatus?identity=${snap.id}&email=${snap.get('email')}`,
                    from: `whatsapp:${dfdapprovednumber}`,
                  }
      
               client.messages.create(orderMessage).then( (message: any) => console.log(message)).catch( (error: any) => console.log(error))
               client.messages.create(orderMessageAdmin).then( (message: any) => console.log(message)).catch( (error: any) => console.log(error))
              //  Hello mate, {{1}} placed an offer of {{2}} on {{3}}. Kindly get in touch on phone: {{4}} or email: {{5}}.
    
                }else if(snap.get('subject') === 'offer'){

                  const offerMessage = {
                    body:`Thank you ${snap.get('name')} for placing an offer of ${snap.get('offerPrice')}${snap.get('currency')} on ${snap.get('carTitle')}. One of our sales team will get back to you soon.`,
                    to: `whatsapp:${res.get('phone')}`,
                    // statusCallback: `https://us-central1-direct-from-dubai.cloudfunctions.net/updateMessageDeliveryStatus?identity=${snap.id}&email=${snap.get('email')}`,
                    from: `whatsapp:${dfdapprovednumber}`,
                  }

                  const offerMessageAdmin = {
                    // Hello mate, ${snap.get('name')} placed an offer of ${snap.get('name')} on ${snap.get('name')}. Kindly get in touch on phone: ${snap.get('phone')} or email: ${snap.get('email')}.
                    body:`Hello mate, ${snap.get('name')} placed an offer of ${snap.get('offerPrice')} on ${snap.get('carTitle')}. Kindly get in touch on phone: ${snap.get('phone')} or email: ${snap.get('email')}.`,
                    to: `whatsapp:+971526186549`,
                    // statusCallback: `https://us-central1-direct-from-dubai.cloudfunctions.net/updateMessageDeliveryStatus?identity=${snap.id}&email=${snap.get('email')}`,
                    from: `whatsapp:${dfdapprovednumber}`,
                  }
      
                  client.messages.create(offerMessage).then( (message: any) => console.log(message)).catch( (error: any) => console.log(error))
                  client.messages.create(offerMessageAdmin).then( (message: any) => console.log(message)).catch( (error: any) => console.log(error))
    
                }else if(snap.get('subject') === 'request'){

                  const reqMessage = {
                    body:`Thank you ${snap.get('name')} for placing a request for ${snap.get('make')} ${snap.get('model')} ${snap.get('variant')}. One of our sales team will get back to you soon.`,
                    to: `whatsapp:${res.get('phone')}`,
                    // statusCallback: `https://us-central1-direct-from-dubai.cloudfunctions.net/updateMessageDeliveryStatus?identity=${snap.id}&email=${snap.get('email')}`,
                    from: `whatsapp:${dfdapprovednumber}`,
                  }

                  const reqMessageAdmin = {
                    body:`Hello mate, ${snap.get('name')} is requesting for ${snap.get('make')} ${snap.get('model')} ${snap.get('variant')} with a budget range of ${snap.get('budgetRange')[0]} - ${snap.get('budgetRange')[1]} GBP. Kindly get in touch on phone: ${snap.get('phone')} or email: ${snap.get('email')}`,
                    to: `whatsapp:+971526186549`,
                    from: `whatsapp:${dfdapprovednumber}`,
                  }
      
               client.messages.create(reqMessage).then( (message: any) => console.log(message)).catch( (error: any) => console.log(error))
               client.messages.create(reqMessageAdmin).then( (message: any) => console.log(message)).catch( (error: any) => console.log(error))
    
                }else {
                  console.log("No Subject Yet")

                  
    
                }
    
              }

             }).catch(err => {
               return console.log(" Erro", err)
             })

          
         

});

exports.updateMessageDeliveryStatus = functions.https.onRequest((request, response) => {

  cors( request, response, () => {

    const statusContent = request.body
    const identity: any = request.query.identity
    const myEmail: any = request.query.email

    console.log('updating', myEmail, 'Chat ID', identity, 'Body', statusContent, 'As ', statusContent.MessageStatus)

    return admin.firestore().collection('customerEngagements').doc(myEmail).collection('chats').doc(identity).update({status:statusContent.MessageStatus})
            .then( updateDubaiEnquiryStatus => console.log(updateDubaiEnquiryStatus))
            .catch( updateDubaiEnquiryStatusError => console.log(updateDubaiEnquiryStatusError))


  })
});

exports.resendAnotherTemplateIfFreeFormError= functions
        .firestore.document(`customerEngagements/{messageId}/chats/{chatId}`)
        .onUpdate((change, context) => {

          if(change.after.get('status')==='undelivered'){

            return admin.firestore().collection('users').doc(change.after.get('customerId')).get()
            .then( resendTemplateSuccess => {
              

              const textMessageAltTemplate = {
                // `${change.after.get('displayName')}, you have a new message from DirectFromDubai: ${change.after.get('message')}`
                body:`${change.after.get('name')}, you have a new message from DirectFromDubai: ${change.after.get('message')}`,
                to: `whatsapp:${resendTemplateSuccess.get('phone')}`,
                statusCallback: `https://us-central1-direct-from-dubai.cloudfunctions.net/updateMessageDeliveryStatus?identity=${change.after.id}&email=${change.after.get('email')}`,
                from: `whatsapp:${dfdapprovednumber}`,
              }

            return client.messages.create(textMessageAltTemplate)
                    .then( (altTemplateMessageSuccess: any) => console.log(altTemplateMessageSuccess))
                    .catch( (altTemplateMessageError: any) => console.log(altTemplateMessageError))

            })
            .catch( resendError =>   console.log(resendError))

            

          }return null

          

});


////////////////////// CHECK IF CAR IS SOLD  HTTP triggar ////////////////////////////////
// exports.checkIfCarIsSold = functions.firestore.document(`vehicles/{vehicleId}`)
//     .onUpdate(async (change) => {
//     // let myUrl = change.after.get('carLink')
//     // Get list of available VPN connections
//     const connections = connectionManager.connections;
//     // Get active VPN connection (if any)
//     const activeConnection = connectionManager.active;
//     // console.log('ACTIVE CONNECTION ', connectionManager.active)
//     // Bring a connection up (will first take down any active connection)
//     connectionManager.up(activeConnection.id);
//     console.log('TURNED ON ', activeConnection);
//     myRequest(change.after.get('carLink'), (error: any, response: any, body: any) => {
//         const respString = body.toString();
//         if (respString.includes('?404')) {
//             console.log(change.after.get('model'), ' Is Sold');
//             return admin.firestore().collection('vehicles').doc(change.after.id).update({ isSold: true })
//                 .then(successfulySold => {
//                 console.log(successfulySold);
//             })
//                 .catch(errorr => {
//                 console.log(errorr);
//             });
//         }
//         else {
//             console.log(change.after.get('model') - change.after.get('engineModel'), ' Is Still Available');
//             return admin.firestore().collection('vehicles').doc(change.after.id).update({ isSold: false })
//                 .then(successfulySold => {
//                 console.log(successfulySold);
//                 // Take a connection down
//                 connectionManager.down(connections[0].id);
//                 console.log('TURNED OFF ', connectionManager.connections[0]);
//             })
//                 .catch(errorr => {
//                 console.log(errorr);
//             });
//         }
//     });

// });


exports.scrappSingleCarDubizzle = functions.firestore.document(`analytics/{analyticsId}`)
    .onCreate( (snap, context) => {
      console.log(snap.get("link"))
    })


exports.parsehubIncommingFeed = functions.https.onRequest((request, response) => {
  cors( request, response, () => {
        const content = request.body
        if(content.status === 'complete'){

          let options = {
            uri: `https://www.parsehub.com/api/v2/runs/${content.run_token}/data`,
            qs: {
                api_key:'tt0O7QxTTa7H',
                format: "json"
            },
            json: true
          };

        requestPromise(options)
        .then( (repos: any[]) => {
          // let mySnack:any[] =  JSON.parse(repos)

          const result = repos.filter((word: any) => word.price);
          const result1 = repos.filter((word: any) => word.images);
          //  let cars:any[] = getObjects(mySnack,'price','')
           console.log('WITH PRICE', result.length);
           console.log('WITH IMAGES', result1.length);
          //  console.log('CARS', cars.length);

          // mySnack.forEach()

        })
        .catch( (err: any) => {
          console.log('ERRA', err);
    });

          // return postmanRequest(`https://www.parsehub.com/api/v2/runs/${content.run_token}/data`,  (error: any, response: any, body: any) => {
          //   console.log('error:', error); // Print the error if one occurred
          //   console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
          //   console.log('body:', body); // Print the HTML for the Google homepage.
          // });

          

          // return myRequest({
          //   uri: `https://www.parsehub.com/api/v2/runs/${content.run_token}/data`,
          //   method: 'GET',
          //   gzip: true,
          //   qs: {
          //     api_key: "tt0O7QxTTa7H",
          //     format: "json"
          //   }
          // }, function(err: any, resp: any, body: any) {
          //   // console.log("STATUS", content.status)
          //   // console.log("RUN TOKEN", content.run_token)
          //   // // console.log("MY ERROR",err);
          //   // console.log("MY RESPONSE",resp);
          //   // console.log("MY BODY",body);
          //   // console.log("EACH CAR: ", body.stringify())
          //   // let cars: any[] = body.toJSON()
          //   // console.log("MODEL", body)
          //   let car = []
          //   car.push(body)
          //   console.log("MODEL", car)
          //   // cars.forEach( eachCar => {
          //   //   console.log("MODEL", eachCar.model)
          //   // })
          //   // let cars:[] = body
          //   //  cars.forEach( eachCar => {
          //   //   console.log("EACH CAR: ", eachCar)
          //   //  })
            
          // })

          // return myRequest({
          //   uri: `https://www.parsehub.com/api/v2/projects/${content.project_token}/last_ready_run/data`,
          //   method: 'GET',
          //   gzip: true,
          //   qs: {
          //     api_key: "tt0O7QxTTa7H",
          //   }
          // }, function(err: any, resp: any, body:any) {
          //   // console.log(body);
          //   extractDataFromRun(body)
          // });

          // return myRequest({
          //   uri: `https://www.parsehub.com/api/v2/runs/${content.run_token}/data`,
          //   method: 'GET',
          //   gzip: true,
          //   qs: {
          //     api_key: "tt0O7QxTTa7H",
          //     format: "json"
          //   }
          // }, function(err: any, resp: any, body: any) {
          //   // extractDataFromRun(body)
          //   // console.log("KWERAaa", body)
          //   // console.log("TYPE OF", typeof body)
          //   console.log("MY DATA: ", getObjects(body,'price','').length)
          // });

          // return phub.getRunData({ run_token: `${content.run_token}`}, function (err: any, phRunData: any) {
          //   console.log("DATA TYPE:", phRunData);
          //   console.log("MY DATA: ", getObjects(phRunData,'price','').length)
          // });

          // curl.get(`https://www.parsehub.com/api/v2/runs/${content.run_token}/data?api_key=tt0O7QxTTa7H&format=json`, function(err: any, response: any, body: any) {
          //   console.log("DATA TYPE:", typeof body);
          //   console.log("RAW DATA:", getObjects(body,'price','').length);
          // });

          // return fetch(`https://www.parsehub.com/api/v2/runs/${content.run_token}/data`)
          // .then((body: any) => {
          //   // console.log("RAW DATA:", typeof body);
          //   console.log("RAW DATA:", body);
          //   // console.log("RAW DATA:", getObjects(body,'price',''));
          // })
          // .catch((err: any) => console.error('error:' + err));

          // request({
          //   uri: 'https://www.parsehub.com/api/v2/projects/{PROJECT_TOKEN}/last_ready_run/data',
          //   method: 'GET',
          //   gzip: true,
          //   qs: {
          //     api_key: "{YOUR_API_KEY}",
          //     format: "csv"
          //   }
          // }, function(err, resp, body) {
          //   console.log(body);
          // });


        }

       

        // console.log("My RAW  data in JSON: ", request.body.rawBody)

        

        

       
    
      })
    });

  // function extractDataFromRun(data:any) {    
  //   // typeof data
  //   // let mycars:any[]= data
  //   // console.log("TYPE OF", data.toString())
  //   // console.log("ALL CARS FEED", j.stringify())
  //   // let allCarsWithPrice = getObjects(data,'price','')
  //   // console.log("CARS WITH PRICE ARE ", allCarsWithPrice.length)
  //   // // let allCarsWithImages: any[] = getObjects(allCarsWithPrice,'images','')
  //   // // console.log("CARS WITH IMAGES ARE ", allCarsWithImages.length)
  // }

   // GET OBJECTS WITH MAKE FROM JSON FILE ////////
  //  function getObjects(obj: any, key: string, val:string) {
  //   console.log('RECEIVED', obj.length);
  //   console.log('KEY', key);
  //   let objects:any[] = []
  //   for (var i in obj) {
  //       if (!obj.hasOwnProperty(i)) continue;
  //       if (typeof obj[i] == 'object') {
  //           objects = objects.concat(getObjects(obj[i], key, val));    
  //       } else 
  //       //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
  //       if (i == key && obj[i] == val || i == key && val == '') { //
  //           objects.push(obj);
  //       } else if (obj[i] == val && key == ''){
  //           //only add if the object is not already in the array
  //           if (objects.lastIndexOf(obj) == -1){
  //               objects.push(obj);
  //           }
  //       }
  //   }
  //   return objects;
  // }

   ////// GET OBJECTS WITH MAKE FROM JSON FILE ////////
  //  function getObjects(obj: any, key: string, val: string) {
  //    console.log("RECEIVED", obj.length )
  //   let objects: any[] = [];
  //   for (var i in obj) {
  //       if (!obj.hasOwnProperty(i)) continue;
  //       if (typeof obj[i] == 'object') {
  //           objects = objects.concat(getObjects(obj[i], key, val));    
  //       } else 
  //       //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
  //       if (i == key && obj[i] == val || i == key && val == '') { //
  //           objects.push(obj);
  //       } else if (obj[i] == val && key == ''){
  //           //only add if the object is not already in the array
  //           if (objects.lastIndexOf(obj) == -1){
  //               objects.push(obj);
  //           }
  //       }
  //   }
  //   return objects;
  // }
      


exports.adminAlertSavedSearch = functions.firestore.document(`searches/{searchId}/carsearches/{carsearchId}`)
    .onCreate((snap, context) => {

      const newSavedSearchAlert = {
        // body: `Dear Richard, you have a new message: ${snap.get('name')} is looking for ${snap.get('make')} ${snap.get('model')} ${snap.get('variant')?snap.get('variant'):''} ${snap.get('yearFrom')?', '+snap.get('yearFrom'):''} ${snap.get('yearTo')?' - '+snap.get('yearTo'):''} ${snap.get('minBudget')?', '+Number(snap.get('minBudget'))+'GBP' : ''} ${snap.get('maxBudget')?' - '+Number(snap.get('maxBudget'))+'GBP':''} ${snap.get('color')?' in '+snap.get('color'):''} Phone: ${snap.get('phone')} Email: ${snap.get('email')}`,
        body: `Hello mate, ${snap.get('name')} is looking for ${snap.get('make')} ${snap.get('model')} ${snap.get('variant')?snap.get('variant'):''} ${snap.get('maxBudget') ? Number(snap.get('maxBudget'))+'GBP':''} ${snap.get('color')?' in '+snap.get('color'):''} Get back using Phone: ${snap.get('phone')} & Email: ${snap.get('email')}`,
        to: `whatsapp:+971526186549`,
        from: `whatsapp:${dfdapprovednumber}`,
       };

       const newSavedSearchResponse = {
        body: `We’ve got it ${snap.get('name')}, our team will get to work straight away to find you a ${snap.get('make')} ${snap.get('model')} ${snap.get('variant')?snap.get('variant'):''} ${snap.get('maxBudget') ? Number(snap.get('maxBudget'))+'GBP':''} ${snap.get('color')?' in '+snap.get('color'):''} and be in touch shortly`,
        to: `whatsapp:${snap.get('phone')}`,
        from: `whatsapp:${dfdapprovednumber}`,
       };

      client.messages.create(newSavedSearchResponse).then((message) => console.log(message)).catch((error) => console.log(error));
      client.messages.create(newSavedSearchAlert).then((message) => console.log(message)).catch((error) => console.log(error));

});

exports.updateLeadId = functions.firestore.document(`users/{userId}`)
    .onCreate((snap, context) => {
       
      if(snap.get('funnel')==='lead'){
        updateMyLeadId(snap.id)
      }

});

function updateMyLeadId(id: string) {

  admin.firestore().collection('users').doc(id).update({uid:id})
  .then(doc => {
        console.log("Updated Lead ID")
      })
  .catch( error => {
        console.log("Error Updating  Lead ID", error)
      })
  }
  



  exports.autoRegisterLead= functions
        .firestore.document(`users/{userId}`)
        .onUpdate((change, context ) => {
          let currentLead: any = change.after.data()

          // firebase.auth().fetchProvidersForEmail("emailaddress@gmail.com")
          
          // console.log(`Is ${change.after.get('displayName')} registered`, admin.auth().getUserByEmail(change.after.get('email')))
          admin.auth().getUserByEmail(change.after.get('email')).then( isAuthResp => {
            console.log(`Is ${change.after.get('displayName')} registered?`, isAuthResp)
          }).catch( isAuthErr => {
            console.log(`${change.after.get('displayName')} is NOT registered - ${isAuthErr}`, isAuthErr)

            if(change.after.get('upgraded')===true){

              deleteCustomerWithId(change.after.get('uid'))
              .then(deleteResponse => {

                console.log(`Deleted ${change.before.get('displayName')}`)
                // currentLead.uid = currentLead.uid
                // currentLead.id = currentLead.i
                currentLead.password = currentLead.email
                currentLead.upgraded = false

                shortenUrl(`https://www.directfromdubai.com/#/pages/login?u=${currentLead.email}&p=${currentLead.email}`)
                .then( autoSignInUrl => {
                  currentLead.signInLink = autoSignInUrl

                  registerLeadNow(currentLead)
                  .then(regResp => {
                    console.log(`Registered ${currentLead.displayName}`)
                    
                    currentLead.uid = regResp.uid
                    currentLead.id = regResp.uid
                    currentLead.registered = true,

                    updateLeadNow(currentLead)
                    .then( updateLeadResp => {

                      console.log(`Updated ${currentLead.displayName}`)
                      console.log(`Upgraded ${currentLead.displayName} succesfully`)
                    })
                    .catch( updateLeadErr => {
                      console.log(updateLeadErr)
                      console.log(`Error Upgading ${currentLead.displayName}: ${updateLeadErr}`)
                    })

                  })
                  .catch( regError => {
                    console.log(regError)

                  })
          
                })
                .catch( error => error)


              })
              .catch(errorDeleting => {
                console.log(errorDeleting)
              })

          }else {
            console.log(`${currentLead.displayName} Cant Upgrade, Already Upgraded`)
          }

          })


         

  });


function deleteCustomerWithId(userId: string) {
  return admin.firestore().collection('users').doc(userId).delete()
}


async function shortenUrl(url: string) {
  const response = await bitly.shorten(url);
  return response.link
}

function registerLeadNow(customer: any) {
  return admin.app().auth().createUser({email: customer.email, password: customer.email})
}

function updateLeadNow(customer: any) {
  return admin.firestore().collection('users').doc(customer.id).set(customer)
}

// regByAdmin


exports.adminRegisterLead= functions
.firestore.document(`users/{userId}`)
.onCreate(snapshot => {
  let currentLead: any = snapshot.data()

  // firebase.auth().fetchProvidersForEmail("emailaddress@gmail.com")
  
  // console.log(`Is ${change.after.get('displayName')} registered`, admin.auth().getUserByEmail(change.after.get('email')))
  admin.auth().getUserByEmail(snapshot.get('email')).then( isAuthResp => {
    console.log(`Is ${snapshot.get('displayName')} registered?`, isAuthResp)
  }).catch( isAuthErr => {
    console.log(`${snapshot.get('displayName')} is NOT registered - ${isAuthErr}`, isAuthErr)

    if(snapshot.get('regByAdmin')){

      deleteCustomerWithId(snapshot.id)
      .then(deleteResponse => {

        console.log(`Deleted ${snapshot.get('displayName')}`)
        currentLead.regByAdmin = false
        // currentLead.id = currentLead.i
        // currentLead.password = currentLead.password
        // currentLead.upgraded = false

        shortenUrl(`https://www.directfromdubai.com/#/pages/login?u=${currentLead.email}&p=${currentLead.password}`)
        .then( autoSignInUrl => {
          currentLead.signInLink = autoSignInUrl

          registerLeadNow(currentLead)
          .then(regResp => {
            console.log(`Registered ${currentLead.displayName}`)
            
            currentLead.uid = regResp.uid
            currentLead.id = regResp.uid
            currentLead.registered = true,

            updateLeadNow(currentLead)
            .then( updateLeadResp => {

              console.log(`Updated ${currentLead.displayName}`)
              console.log(`Upgraded ${currentLead.displayName} succesfully`)
            })
            .catch( updateLeadErr => {
              console.log(updateLeadErr)
              console.log(`Error Upgading ${currentLead.displayName}: ${updateLeadErr}`)
            })

          })
          .catch( regError => {
            console.log(regError)

          })
  
        })
        .catch( error => error)


      })
      .catch(errorDeleting => {
        console.log(errorDeleting)
      })

  }else {
    console.log(`${currentLead.displayName}, was not reg by admin`)
  }

  })


 

});


exports.correctLikelihoods= functions
      .firestore.document(`users/{userId}`)
      .onCreate( snapshot => {

      let  myNewBatch = admin.firestore().batch()
        
        admin.firestore().collection("users").get()
        .then( getCustomersResp => {
          
          getCustomersResp.forEach( eachCust => {
            let newStage: string = 'Mild';
            let custReff: FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>;

            if(eachCust.get('createdDate')){

              console.log(`Skipped ${eachCust.get('displayName')} since he was updated already`)

            }else {
              // console.log(`${eachCust.get('displayName')} updated as MILD`)

              // if(eachCust.get('temperature')==='extra hot'){

              //   newStage = 'Extra Spicy'
              //   // updateCustomersLikelihood(eachCust.get('uid'), newStage)
              //   custReff = admin.firestore().collection('users').doc(eachCust.get('uid'))
              //   myNewBatch.update(custReff, {likelihood: newStage})
              //   console.log(`Updated ${eachCust.get('displayName')} on a batch`)

              // }else if(eachCust.get('temperature')==='hot'){

              //   newStage = 'Spicy'
              //   // updateCustomersLikelihood(eachCust.get('uid'), newStage)
              //   custReff = admin.firestore().collection('users').doc(eachCust.get('uid'))
              //   myNewBatch.update(custReff, {likelihood: newStage})
              //   console.log(`Updated ${eachCust.get('displayName')} on a batch`)
                
              // }else if(eachCust.get('temperature')==='warm'){

              //   newStage = 'Medium'
              //   // updateCustomersLikelihood(eachCust.get('uid'), newStage)
              //   custReff = admin.firestore().collection('users').doc(eachCust.get('uid'))
              //   myNewBatch.update(custReff, {likelihood: newStage})
              //   console.log(`Updated ${eachCust.get('displayName')} on a batch`)

              // }else {

                // newStage = 'Mild'
                // updateCustomersLikelihood(eachCust.get('uid'), newStage)
                custReff = admin.firestore().collection('users').doc(eachCust.get('uid'))
                myNewBatch.update(custReff, {likelihood: newStage})
                console.log(`Updated ${eachCust.get('displayName')} on a batch`)
                
              // }

              

            }
            
            

          })

          myNewBatch.commit().then( batchWrite => {
            console.log(`Success Updating ${batchWrite.length} records`)

          }).catch( batchWriteErr => {

            console.log(`Error Updating in batch ${batchWriteErr}`)

          })

        }).catch( erra => {
          console.log(`${erra}`)
        })
    

});

// function updateCustomersLikelihood(userId: string, stage: string){
//   // admin.firestore().collection('users').doc(userId).update({carSuggestions: cars, isRead: false})
//   let custReff = admin.firestore().collection('users').doc(userId)

//   return myNewBatch.update(custReff, {likelihood: stage})

//   // return admin.firestore().collection("users").doc(userId).update({likelihood: stage})

// }

exports.showHiddenMessages= functions
      .firestore.document(`customerEngagements/{messageId}`)
      .onCreate( snapshot => {

      let  myNewBatch = admin.firestore().batch()
        
        admin.firestore().collectionGroup("chats").get()
        .then( getCustomerEngResp => {

          console.log(`Skipped ${getCustomerEngResp.size} engagement`)

          // let myuniqueChats: any[] = getUnique(getCustomerEngResp.docs,'email')

          getCustomerEngResp.docs.forEach(eachEng => {

            let myEnquiry: any = eachEng.data()
            

              if(myEnquiry.request){
                
                console.log(`Not a request for ${myEnquiry.email}`)
               
              }else {
                

                let myMessage = {
                  createdAt: myEnquiry.createdAt,
                  email: myEnquiry.email,
                  isActive: false,
                  isRead: myEnquiry.isRead,
                  lastMessage: myEnquiry.message,
                  mediaType: myEnquiry.mediaType,
                  mediaUrl: myEnquiry.mediaUrl || '',
                  name: myEnquiry.name,
                  onlineStatus:'offline',
                  phone: myEnquiry.phone || '',
                  unreadCounter: messagesUnreadCounter(myEnquiry.email),
                  userId: myEnquiry.customerId || myEnquiry.email
              }

              let custEngReff: FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData> = admin.firestore().collection('customerEngagements').doc(myEnquiry.email)
              myNewBatch.set(custEngReff, myMessage)


              }

               

          })

          myNewBatch.commit().then( batchWrite => {
            console.log(`Success Updating ${batchWrite.length} records`)

          }).catch( batchWriteErr => {

            console.log(`Error Updating in batch ${batchWriteErr}`)

          })

        }).catch( erra => {
          console.log(`${erra}`)
        })
    

});



function messagesUnreadCounter(email: string): number {
  let count: number = 1

  admin.firestore().collection('customerEngagements').doc(email).collection('chats').where('isRead','==', false).get()
  .then( resp => {
    count = resp.size
  })
  .catch(respError => {
    // count = 1
  })

  return count
}


// function getUnique(enq: any[], column: string) {

//   // store the comparison  values in array
// const unique =  enq.map(e => e[column])

// // store the indexes of the unique objects
// .map((e, i, final) => final.indexOf(e) === i && i)

// // eliminate the false indexes & return unique objects
// .filter((e) => enq[e]).map(e => enq[e]);

// return unique;

// }


  exports.addLastUpdatedOnEveryCustomer= functions
        .firestore.document(`users/{userId}`)
        .onCreate(snap => {
          
          let  myNewBatch = admin.firestore().batch()
          admin.firestore().collection('users').get()
          .then( usersResp => {
            usersResp.forEach( eachUser => {

              if(eachUser.get('createdDate')){

                if(eachUser.get('lastUpdated')){

                  console.log(eachUser.get('displayName'), 'already updated')

                } else {

                  let custEngReff: FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData> = admin.firestore().collection('users').doc(eachUser.id)
                  myNewBatch.update(custEngReff, {lastUpdated: eachUser.get('createdDate')})

                  
                }

              }else {

                console.log("Old Customer", eachUser.get('displayName'))

              }

              myNewBatch.commit().then( comitResp => {
                console.log(comitResp.length, "Records Updated")
              }).catch( myeRR => {
                console.log(myeRR)
              })

            })

          }).catch( error => {
            console.log(error)
          }) 

  });



  exports.addContact = functions
      .firestore.document(`users/{userId}`)
      .onCreate( snapshot => {
        /////// Create contacts ////////////
        let customer = [{
          "email":snapshot.get('email'),
          "country":snapshot.get('country'),

        }]

        const url = `https://app-server.wati.io/api/v1/addContact/${snapshot.get('phone')}`;
        const options = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json-patch+json',
            Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIyNDczZDhmZS0zYjNiLTRjOGYtYjk0NC1jYzZmZWQ4OTZlMTciLCJ1bmlxdWVfbmFtZSI6ImVsa2FuYXJvcEBnbWFpbC5jb20iLCJuYW1laWQiOiJlbGthbmFyb3BAZ21haWwuY29tIiwiZW1haWwiOiJlbGthbmFyb3BAZ21haWwuY29tIiwiYXV0aF90aW1lIjoiMTEvMTgvMjAyMSAwNjo0ODoyMSIsImRiX25hbWUiOiJ3YXRpX2FwcCIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IlRSSUFMIiwiZXhwIjoxNjM3ODg0ODAwLCJpc3MiOiJDbGFyZV9BSSIsImF1ZCI6IkNsYXJlX0FJIn0.kINfk-7IIhFypux4sxSh2XSSM1UdEgQfCQoDoxWOYgQ'
          },
          body: `{"customParams":${customer},"name":${snapshot.get('displayName')}}`
        };

        fetch(url, options)
          .then((res: any) => res.json())
          .then((json: any) => console.log(json))
          .catch((err: any) => console.error('error:' + err));

});

exports.getMessageTemplates = functions
.firestore.document(``)
.onCreate( snapshot => {
  /////// getMessageTemplates ////////////
  const url = 'https://app-server.wati.io/api/v1/getMessageTemplates?pageSize=10&pageNumber=1';
  const options = {
    method: 'GET',
    headers: {
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIyNDczZDhmZS0zYjNiLTRjOGYtYjk0NC1jYzZmZWQ4OTZlMTciLCJ1bmlxdWVfbmFtZSI6ImVsa2FuYXJvcEBnbWFpbC5jb20iLCJuYW1laWQiOiJlbGthbmFyb3BAZ21haWwuY29tIiwiZW1haWwiOiJlbGthbmFyb3BAZ21haWwuY29tIiwiYXV0aF90aW1lIjoiMTEvMTgvMjAyMSAwNjo0ODoyMSIsImRiX25hbWUiOiJ3YXRpX2FwcCIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IlRSSUFMIiwiZXhwIjoxNjM3ODg0ODAwLCJpc3MiOiJDbGFyZV9BSSIsImF1ZCI6IkNsYXJlX0FJIn0.kINfk-7IIhFypux4sxSh2XSSM1UdEgQfCQoDoxWOYgQ'
    }
  };

  fetch(url, options)
    .then((res: any) => res.json())
    .then((json: any) => console.log(json))
    .catch((err: any) => console.error('error:' + err));

});



////////////////////////// PARSEHUB //////////////////////
exports.incomingParsehubScrapper = functions.https.onRequest((request, response) => {
  cors( request, response, async () => {

        const content = request.body
        if(content.status === 'complete'){
         
         await myRequest.post({
             url: 'https://8098-41-90-66-225.ngrok.io/data-for-run/tt0O7QxTTa7H/tTLGLHavXg6f',
             form: { myToken: content.run_token}
           },
             (err: any, httpResponse: any, body: any) => {
             if(err){
              console.log("Error sending token ", content.run_token , 'to your data cleaning script')
             }else {
              console.log("Sent token ", content.run_token , 'to your data cleaning script')
             }
           }
         )
          
          // postData(`https://01d3-41-90-70-51.ngrok.io/data-for-run/tt0O7QxTTa7H/tTLGLHavXg6f`, { myToken: content.run_token })
          // .then(data => {
          //   console.log(data); // JSON data parsed by `data.json()` call
          //   console.log("Sent token ", content.run_token , 'to your data cleaning script')
          // }).catch( err => console.log("Error sending token ", content.run_token , 'to your data cleaning script'))

        }else console.log("Still waiting to call Data Cleaning Script: ", content.status)
    
      })
});


// // Example POST method implementation:
// async function postData(url = '', data = {}) {
//       // Default options are marked with *
//       const response = await fetch(url, {
//         method: 'POST', // *GET, POST, PUT, DELETE, etc.
//         mode: 'cors', // no-cors, *cors, same-origin
//         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
//         credentials: 'same-origin', // include, *same-origin, omit
//         headers: {
//           'Content-Type': 'application/json'
//           // 'Content-Type': 'application/x-www-form-urlencoded',
//         },
//         redirect: 'follow', // manual, *follow, error
//         referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
//         body: JSON.stringify(data) // body data type must match "Content-Type" header
//       });
//       return response.json(); // parses JSON response into native JavaScript objects
//   }


function updateNotesForNewLeads(lead: any) {

  return admin.firestore().collection('notes').add(lead)

}